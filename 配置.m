// Add Here an image to show
[progressView setPhotoImage:[UIImage imageNamed:@"flower"]];

// Additionally you can set the message at any time (Default: Uploading...)
[progressView setUploadMessage:@"Uploading..."];

// You can customize the progress tint color
[progressView setProgressTintColor:[UIColor whiteColor]];

// Additionally you can customize the progress track color
[progressView setProgressTrackColor:[UIColor darkGrayColor]];