WDUploadProgressView 是 iOS 文件上传进度视图控件，通过在 UITableView 上的头部显示文件上传的进度，并及时根据上传的进度来显示更新。

github地址: git://github.com/sprint84/WDUploadProgressView.git
